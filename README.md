# Identifying Grammer

Step1 - download the publicly available data, since the given data is not labelled.

Step2 - Define the architecture of an LSTM model and train on the downloaded data.

Step3 - Predict the class and probability of the a sentence given grammatecally correct or not.

The performance of  the model seems to be too poor, accounting for the corpus trained and the complexity of LSTM model due to limited computational facilities.


